package com.example.jpql.service;

import com.example.jpql.entity.Student;
import com.example.jpql.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentRepository studentRepository;
    public List<Student> getStudentlist()
    {
        return studentRepository.findAll();
    }
    public void create(Student student)
    {
        studentRepository.save(student);
    }
    public List<Student> getSortedList()
    {
        return studentRepository.sortByCgpa();
    }
}
