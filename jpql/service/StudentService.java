package com.example.jpql.service;

import com.example.jpql.entity.Student;

import java.util.List;

public interface StudentService {
    public List<Student> getStudentlist();
    public void create(Student student);
    public List<Student> getSortedList();
}
