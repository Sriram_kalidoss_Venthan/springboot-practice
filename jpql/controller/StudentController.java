package com.example.jpql.controller;

import com.example.jpql.entity.Student;
import com.example.jpql.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping("/get")
    public List<Student> get()
    {
        return studentService.getStudentlist();
    }
    @PostMapping("/add")
    public String add(@RequestBody Student student)
    {
        studentService.create(student);
        return "created";
    }
    @GetMapping("/sort_by_cgpa")
    public List<Student> getSortedList()
    {
        return studentService.getSortedList();
    }
}
