package com.example.jpql.entity;

import javax.persistence.*;

@Entity
@Table(name="Student")
@NamedQuery(name="Student.sortByCgpa",query="SELECT e FROM Student e ORDER BY e.cgpa DESC")
public class Student {
    @Id
    @GeneratedValue
    @Column(name="student_id")
    private long id;
    @Column(name ="name")
    private String name;
    @Column(name = "cgpa")
    private double cgpa;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCgpa() {
        return cgpa;
    }

    public void setCgpa(double cgpa) {
        this.cgpa = cgpa;
    }
}
