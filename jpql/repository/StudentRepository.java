package com.example.jpql.repository;

import com.example.jpql.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query("SELECT e FROM Student e ORDER BY e.cgpa DESC")
public List<Student> sortByCgpa();
}
